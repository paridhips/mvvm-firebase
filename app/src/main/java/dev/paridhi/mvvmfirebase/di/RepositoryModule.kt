package dev.paridhi.mvvmfirebase.di

import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dev.paridhi.mvvmfirebase.data.repository.NoteRepository
import dev.paridhi.mvvmfirebase.data.repository.NoteRepositoryImp
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {
    @Provides
    @Singleton
    fun provideNoteRepository(
        database:FirebaseFirestore
    ):NoteRepository{
        return  NoteRepositoryImp(database)
    }
}