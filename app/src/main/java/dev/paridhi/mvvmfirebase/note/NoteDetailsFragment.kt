package dev.paridhi.mvvmfirebase.note

import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import dev.paridhi.mvvmfirebase.data.model.Note
import dev.paridhi.mvvmfirebase.databinding.FragmentNoteDetailsBinding
import dev.paridhi.mvvmfirebase.util.UiState
import dev.paridhi.mvvmfirebase.util.hide
import dev.paridhi.mvvmfirebase.util.show
import dev.paridhi.mvvmfirebase.util.toast
import java.util.Date

@AndroidEntryPoint
class NoteDetailsFragment : Fragment() {

    val TAG: String="NotesDetailFragment"
    lateinit var binding: FragmentNoteDetailsBinding
    val viewModel:NoteViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNoteDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.saveBtn.setOnClickListener{
            if(validation())
            {
                viewModel.addNote(
                    Note(
                        id="",
                        text = binding.noteEt.text.toString(),
                        date = Date()
                    )
                )

                viewModel.addnote.observe(viewLifecycleOwner){state ->

                    when(state)
                    {
                        is UiState.Loading->
                        {
                            binding.addNoteProgress.show()

                        }

                        is UiState.Success->
                        {
                            binding.addNoteProgress.hide()
                            toast("Note has been saved")

                        }
                        is UiState.Failure->
                        {
                            binding.addNoteProgress.hide()
                            toast("Error: "+state.error)

                        }
                    }
                }

            }
        }
    }

    private fun validation(): Boolean{
        var isValid=true
        if(binding.noteEt.text.toString().isNullOrEmpty())
        {
            isValid=false
            toast("Enter the body")
        }
        return isValid
    }


}