package dev.paridhi.mvvmfirebase.note

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import dev.paridhi.mvvmfirebase.R
import dev.paridhi.mvvmfirebase.databinding.FragmentNoteListingBinding
import dev.paridhi.mvvmfirebase.util.UiState
import dev.paridhi.mvvmfirebase.util.hide
import dev.paridhi.mvvmfirebase.util.show
import dev.paridhi.mvvmfirebase.util.toast

@AndroidEntryPoint
class NoteListingFragment : Fragment() {

    val TAG: String="NotesListingFragment"
    lateinit var binding: FragmentNoteListingBinding
    val viewModel:NoteViewModel by viewModels()
    val adapter by lazy {
        NoteListingAdapter(
            onItemClicked = { pos, item ->

            },
            onEditItemClicked = { pos, item ->

            },
            onDeleteItemClicked = { pos, item ->
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding=FragmentNoteListingBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.createNewNoteBtn.setOnClickListener {
            findNavController().navigate(R.id.action_noteListingFragment_to_noteDetailsFragment)
        }
        binding.notesListRv.adapter=adapter
        viewModel.getNotes()
        viewModel.note.observe(viewLifecycleOwner){state->
            when(state){
                is UiState.Loading->{
                    Log.e("Data","Loading")
                    binding.noteListProgressBar.show()

                }
                is UiState.Failure->{
                    binding.noteListProgressBar.hide()
                    toast(state.error)

                }
                is UiState.Success->{
                    binding.noteListProgressBar.hide()
                    state.data.forEach {
                        adapter.updateList(state.data.toMutableList())
                    }

                }
            }

        }
    }
}