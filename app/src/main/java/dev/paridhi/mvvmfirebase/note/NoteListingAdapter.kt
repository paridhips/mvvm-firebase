package dev.paridhi.mvvmfirebase.note

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.paridhi.mvvmfirebase.data.model.Note
import dev.paridhi.mvvmfirebase.databinding.LayoutNoteItemBinding

class NoteListingAdapter(
    var onItemClicked:(Int, Note)->Unit,
    var onEditItemClicked:(Int, Note)->Unit,
    var onDeleteItemClicked: (Int, Note)->Unit,
) : RecyclerView.Adapter<NoteListingAdapter.ViewHolder>(){
    private var list: MutableList<Note> = arrayListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView=LayoutNoteItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item=list.get(position)
        holder.bind(item)

    }
    fun updateList(list:MutableList<Note>)
    {
        this.list=list
        notifyDataSetChanged()

    }
    fun removeItem(position:Int){
        list.removeAt(position)
        notifyDataSetChanged()
    }
   inner class ViewHolder(val binding: LayoutNoteItemBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(item:Note)
        {
            binding.idTv.setText(item.id)
            binding.textTv.setText(item.text)
            binding.dateTv.setText(item.date.toString())
            binding.editBtn.setOnClickListener{ onEditItemClicked.invoke(adapterPosition,item) }
            binding.deleteBtn.setOnClickListener { onDeleteItemClicked.invoke(adapterPosition,item) }
            binding.noteItem.setOnClickListener{onItemClicked.invoke(adapterPosition,item)}

        }

    }


}