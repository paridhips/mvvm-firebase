package dev.paridhi.mvvmfirebase.note

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.paridhi.mvvmfirebase.data.model.Note
import dev.paridhi.mvvmfirebase.data.repository.NoteRepository
import dev.paridhi.mvvmfirebase.util.UiState
import javax.inject.Inject

@HiltViewModel
class NoteViewModel @Inject constructor(
    val repository: NoteRepository
):ViewModel() {

    private val _notes=MutableLiveData<UiState<List<Note>>>()
    val note:LiveData<UiState<List<Note>>>
        get() = _notes

    private val _addnotes=MutableLiveData<UiState<String>>()
    val addnote:LiveData<UiState<String>>
        get() = _addnotes

    fun getNotes(){
        _notes.value=UiState.Loading
        repository.getNotes {
            _notes.value=it
        }
    }
    fun addNote(note: Note)
    {
        _addnotes.value=UiState.Loading
        repository.addNote(note){
            _addnotes.value=it
        }
    }


}