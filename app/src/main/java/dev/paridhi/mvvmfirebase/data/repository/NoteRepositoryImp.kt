package dev.paridhi.mvvmfirebase.data.repository

import com.google.firebase.firestore.FirebaseFirestore

import dev.paridhi.mvvmfirebase.data.model.Note
import dev.paridhi.mvvmfirebase.util.FireStoreCollections
import dev.paridhi.mvvmfirebase.util.UiState


class NoteRepositoryImp(
    val database:FirebaseFirestore
):NoteRepository {

    override fun getNotes(result: (UiState<List<Note>>) -> Unit) {
        //get data from firestore
       database.collection(FireStoreCollections.NOTE)
           .get()
           .addOnSuccessListener {
               val notes= arrayListOf<Note>()
               for(document in it)
               {
                   val note=document.toObject(Note::class.java)
                   notes.add(note)

               }
               result.invoke(
                   UiState.Success(notes)
               )


           }
           .addOnFailureListener{
               result.invoke(UiState.Failure(it.message))

           }
    }

    override fun addNote(note: Note, result: (UiState<String>) -> Unit) {
        database.collection(FireStoreCollections.NOTE).add(note).addOnSuccessListener {
            result.invoke(
                UiState.Success(it.id)
            )
        }.addOnFailureListener{
            result.invoke(UiState.Failure(it.message))

        }

    }
}