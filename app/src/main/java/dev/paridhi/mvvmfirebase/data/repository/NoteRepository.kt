package dev.paridhi.mvvmfirebase.data.repository

import dev.paridhi.mvvmfirebase.data.model.Note
import dev.paridhi.mvvmfirebase.util.UiState

interface NoteRepository {
    fun getNotes(result: (UiState<List<Note>>)->Unit)
    fun addNote(note: Note,result:(UiState<String>)->Unit)
}